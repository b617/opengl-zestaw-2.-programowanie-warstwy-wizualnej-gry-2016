#include "stdafx.h"
#include "Mesh.h"


Mesh::Mesh(Vertex* vertices, unsigned int numVertices, GLuint drawingMode) : mDrawingMode(drawingMode), mDrawCount(numVertices)
{
	glGenVertexArrays(1, &mVertexArrayObject);
	glBindVertexArray(mVertexArrayObject);

		glGenBuffers(NUM_BUFFERS, mVertexArrayBuffers);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(vertices[0]),vertices,GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}

Mesh::Mesh(std::vector<Vertex> & vertices, GLuint drawingMode) : mDrawingMode(drawingMode)
{
	glGenVertexArrays(1, &mVertexArrayObject);
	glBindVertexArray(mVertexArrayObject);

		glGenBuffers(NUM_BUFFERS, mVertexArrayBuffers);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}


Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &mVertexArrayObject);
}

void Mesh::Draw() {
	glBindVertexArray(mVertexArrayObject);
		
		glDrawArrays(mDrawingMode, 0, mDrawCount);
		
	glBindVertexArray(0);
}

Mesh & Mesh::setDrawingMode(GLuint value) {
	mDrawingMode = value;
	return *this;
}

Mesh * BASIC_SHAPES::shapes2D::RegularPolygon(float radius, unsigned int verticesCount){
	float angle = M_PI * 2 / verticesCount, currentAngle = 0.0f;
	Vertex * vertices = (Vertex*)malloc(sizeof(Vertex)*verticesCount);

	for (unsigned int i = 0; i < verticesCount; i++, currentAngle += angle) {
		vertices[i] = Vertex(glm::vec3(radius*cos(currentAngle), radius*sin(currentAngle), 0.0f));
	}

	return new Mesh(vertices, verticesCount, GL_TRIANGLE_FAN);
}

Mesh * BASIC_SHAPES::shapes2D::Square(float radius){
	return RegularPolygon(radius, 4);
}

Mesh * BASIC_SHAPES::shapes2D::Triangle(float radius){
	return RegularPolygon(radius, 3);
}