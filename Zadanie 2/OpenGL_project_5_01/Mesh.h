#pragma once
#include "glm\glm.hpp"
#include "gl3w.h"
#include <vector>
#include <string>
#include "Shader.h"

#define M_PI 3.14159265358979323846
class Vertex;

class Mesh
{
public:
	Mesh(Vertex* vertices, unsigned int numVertices, GLuint drawingMode = GL_TRIANGLES);
	Mesh(std::vector<Vertex> & vertices, GLuint drawingMode = GL_TRIANGLES);

	void Draw();

	Mesh & setDrawingMode(GLuint value);

	virtual ~Mesh();
protected:
	enum
	{
		POSITION_VB,

		NUM_BUFFERS
	};
	GLuint mVertexArrayObject;
	GLuint mVertexArrayBuffers[NUM_BUFFERS];
	unsigned int mDrawCount;
	GLuint mDrawingMode = GL_TRIANGLES;
};

class Vertex{
public:
	Vertex(const glm::vec3& position) : pos(position){ }
	std::string toString(){
		return std::to_string(pos.x) + ", " + std::to_string(pos.y) + ", " + std::to_string(pos.z);
	}
	glm::vec3 getPosition(){
		return pos;
	}
protected:
	glm::vec3 pos;
};

struct MeshWithShader{
	Mesh * mesh;
	Shader * shader;
};

class MeshStructure
{
public:
	MeshStructure(){}
	std::vector<MeshWithShader> objects;
	void Draw(){
		for each (MeshWithShader var in objects) {
			if (var.shader != NULL) var.shader->Bind();
			var.mesh->Draw();
		}
	}

	virtual ~MeshStructure(){}	
};

namespace BASIC_SHAPES {
	namespace shapes2D {

		Mesh * RegularPolygon(float radius, unsigned int verticesCount);
		Mesh * Square(float radius);
		Mesh * Triangle(float radius);

	}
	namespace shapes3D{

	}
}