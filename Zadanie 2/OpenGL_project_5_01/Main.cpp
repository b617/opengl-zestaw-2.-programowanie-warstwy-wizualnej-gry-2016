#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>
#include "glm\gtx\transform.hpp"

//---------------------------------------------------------
#define GREEN 0.0f, 1.0f, 0.0f
#define RED 1.0f, 0.0f, 0.0f

void StartGL();
void QuitGL();

Shader * fullColorShader;
GLint shader_colorPosition;
GLint shader_transformationPosition;

bool pierwszyWzor = true;

void SetDrawingColor(float r, float g, float b){
	glUniform3f(shader_colorPosition, r, g, b);
	fullColorShader->Bind();
}

void ApplyTransformations(float translateX, float translateY, float rotationAngle, float scale){
	glm::mat4 transformation = glm::translate(glm::vec3(translateX, translateY, 0.0f))
							 * glm::rotate(rotationAngle, glm::vec3(0.0f, 0.0f, 1.0f))
							 * glm::scale(glm::vec3(scale, scale, 1.0f));
	glUniformMatrix4fv(shader_transformationPosition, 1, GL_FALSE, &transformation[0][0]);
	fullColorShader->Bind();
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//std::cout << key << " " << scancode << " " << action << " " << mods <<std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	else if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) pierwszyWzor = !pierwszyWzor;
}

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	fullColorShader = new Shader("shaders/myShader");
	shader_colorPosition = glGetUniformLocation(fullColorShader->GetProgramID(), "inColor");
	shader_transformationPosition = glGetUniformLocation(fullColorShader->GetProgramID(), "transform");

	Vertex verticesSquare[] = { Vertex(glm::vec3(-.5, .5, 0)), Vertex(glm::vec3(-.5, -.5, 0)), Vertex(glm::vec3(.5, -.5, 0)), Vertex(glm::vec3(.5, .5, 0)) };
	Vertex verticesTriangle[] = { Vertex(glm::vec3(0, .25, 0)), Vertex(glm::vec3(-.25, -.25, 0)), Vertex(glm::vec3(.25, -.25, 0)) };
	Mesh square(verticesSquare, 4, GL_TRIANGLE_FAN);
	Mesh triangle(verticesTriangle, 3);
	
	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////
		float positionX = 0.0f, positionY = 0.0f, rotationAngle = 0.0f, scale = .2f;

		if (pierwszyWzor){
			SetDrawingColor(GREEN);
			rotationAngle = M_PI / 4;
			for (positionX = -0.8f; positionX < 1.0f; positionX += 0.4f){
				for (positionY = -0.8f; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					square.Draw();
				}
			}

			float leftUp = -0.91f, rightDown = -0.69f;
			SetDrawingColor(RED);
			for (positionX = rightDown; positionX < 1.0f; positionX += 0.4f){
				for (positionY = leftUp; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

			rotationAngle = M_PI * 0.75;
			for (positionX = rightDown; positionX < 1.0f; positionX += 0.4f){
				for (positionY = rightDown; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

			rotationAngle = M_PI * 1.25;
			for (positionX = leftUp; positionX < 1.0f; positionX += 0.4f){
				for (positionY = rightDown; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

			rotationAngle = M_PI * 1.75;
			for (positionX = leftUp; positionX < 1.0f; positionX += 0.4f){
				for (positionY = leftUp; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

		}
		else{
			SetDrawingColor(GREEN);
			for (positionX = -0.8f; positionX < 1.0f; positionX += 0.4f){
				for (positionY = -0.8f; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					square.Draw();
				}
			}

			SetDrawingColor(RED);

			for (positionX = -0.8f; positionX < 1.0f; positionX += 0.4f){
				for (positionY = -0.95f; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

			rotationAngle = M_PI / 2;
			for (positionX = -0.65f; positionX < 1.0f; positionX += 0.4f){
				for (positionY = -0.8f; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

			rotationAngle = M_PI;
			for (positionX = -0.8f; positionX < 1.0f; positionX += 0.4f){
				for (positionY = -0.65f; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}

			rotationAngle = 1.5 * M_PI;
			for (positionX = -0.95f; positionX < 1.0f; positionX += 0.4f){
				for (positionY = -0.8f; positionY < 1.0f; positionY += 0.4f){
					ApplyTransformations(positionX, positionY, rotationAngle, scale);
					triangle.Draw();
				}
			}
		}
		
		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0, 0, 0, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}