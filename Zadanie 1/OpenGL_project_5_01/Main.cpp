#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

//---------------------------------------------------------
void StartGL();
void QuitGL();

MeshStructure ** meshes = new MeshStructure*[3];
MeshStructure * meshDrawn;
enum MESHE{
	KULA,
	CYLINDER,
	STOZEK,
	KONIEC
};
int m = KULA;

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//std::cout << key << " " << scancode << " " << action << " " << mods <<std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	else if (key == GLFW_KEY_W) Application::GetInstance().keyW = action == GLFW_PRESS;
	else if (key == GLFW_KEY_S) Application::GetInstance().keyS = action == GLFW_PRESS;
	else if (key == GLFW_KEY_A) Application::GetInstance().keyA = action == GLFW_PRESS;
	else if (key == GLFW_KEY_D) Application::GetInstance().keyD = action == GLFW_PRESS;

	else if (key == GLFW_KEY_UP) Application::GetInstance().keyUp = action == GLFW_PRESS;
	else if (key == GLFW_KEY_DOWN) Application::GetInstance().keyDown = action == GLFW_PRESS;
	else if (key == GLFW_KEY_LEFT) Application::GetInstance().keyLeft = action == GLFW_PRESS;
	else if (key == GLFW_KEY_RIGHT) Application::GetInstance().keyRight = action == GLFW_PRESS;

	else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS){
		m++;
		switch (m)
		{
		case KULA:
			meshDrawn = meshes[KULA];
			break;
		case CYLINDER:
			meshDrawn = meshes[CYLINDER];
			break;
		case STOZEK:
			meshDrawn = meshes[STOZEK];
			break;
		default:
			meshDrawn = meshes[KULA];
			m = KULA;
		}
	}
}


int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	meshes[KULA] = BASIC_SHAPES::shapes3D::Sphere(0.5f, 32);
	meshes[CYLINDER] = BASIC_SHAPES::shapes3D::Cylinder(0.5f, 1.0f, 32);
	meshes[STOZEK] = BASIC_SHAPES::shapes3D::Cone(0.5f, 1.0f, 32);

	meshDrawn = meshes[KULA];


	Shader shader("shaders/myShader");
	GLuint shader_inColorLocation = glGetUniformLocation(shader.GetProgramID(), "inColor");
	GLuint shader_transformLocation = glGetUniformLocation(shader.GetProgramID(), "transform");

	

	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////
		glUniform3f(shader_inColorLocation, 1.0, 1.0, 1.0);
		glm::mat4 transform = application.PerspectiveMatrix()*application.MoveCamera()*glm::mat4(1.0f);
		glUniformMatrix4fv(shader_transformLocation, 1, GL_FALSE, &transform[0][0]);

		shader.Bind();
		meshDrawn->Draw();


		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	//glfwSetCursorPosCallback(application.window, mouse_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0, 0, 0, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}