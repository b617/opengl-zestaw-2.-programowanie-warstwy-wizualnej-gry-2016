#pragma once
#include <vector>
#include <GLFW\glfw3.h>
#include "glm\glm.hpp"
#include "Sprite.h"
class Application{
private:
	Application(){
	}
	~Application(){
		for each (Sprite* s in AllSprites)
		{
			s->~Sprite();
		}
		AllSprites.clear();
	}
public:
	static Application & GetInstance(){
		static Application instance;
		return instance;
	}
	Application(Application const&) = delete;
	void operator=(Application const&) = delete;

	GLFWwindow * window;
	int windowSizeX = 800, windowSizeY = 800;
	float windowRatio = 1.0f * windowSizeX / windowSizeY;
	std::string windowTitle = "OpenGL window with GLFW, gl3w and glm";
	bool CreateNewWindow(){

		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

		glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		window = glfwCreateWindow(windowSizeX, windowSizeY, windowTitle.c_str(), NULL, NULL);
		return (window != NULL);
	}

	bool keyW = false, keyS = false, keyA = false, keyD = false;
	bool keyUp = false, keyDown = false, keyLeft = false, keyRight = false;
	glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
	glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	float cameraMovementSpeed = 0.03f;
	float cameraRotationSpeed = 0.3f;
	glm::vec2 mousePosAtLastFrame = glm::vec2(0.5f*windowSizeX, 0.5f*windowSizeY);
	glm::vec2 cameraYawAndPitch = glm::vec2(-89.0f, 0.0f);
	bool jumpAtStartHappened = false;

	std::vector<Sprite*> AllSprites;
	void AddSprite(Sprite * const s){
		AllSprites.push_back(s);
	}

	glm::mat4 MoveCamera(){
		if (keyW)
			cameraPos += cameraMovementSpeed * cameraFront;
		if (keyS)
			cameraPos -= cameraMovementSpeed * cameraFront;
		if (keyA)
			cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraMovementSpeed;
		if (keyD)
			cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraMovementSpeed;
		if (keyUp)
			cameraYawAndPitch.y += cameraRotationSpeed;
		if (keyDown)
			cameraYawAndPitch.y -= cameraRotationSpeed;
		if (keyRight)
			cameraYawAndPitch.x += cameraRotationSpeed;
		if (keyLeft)
			cameraYawAndPitch.x -= cameraRotationSpeed;

		glm::vec3 newFront;
		newFront.x = 0.0f;
		newFront.x = cos(glm::radians(cameraYawAndPitch.y)) * cos(glm::radians(cameraYawAndPitch.x));
		newFront.y = sin(glm::radians(cameraYawAndPitch.y));
		newFront.z = cos(glm::radians(cameraYawAndPitch.y)) * sin(glm::radians(cameraYawAndPitch.x));
		cameraFront = glm::normalize(newFront);
		/*fputs((std::to_string(cameraFront.x) + ", " + std::to_string(cameraFront.y) + ", " + std::to_string(cameraFront.z)).c_str(), stdout);
		fputs((" :: "+std::to_string(cameraYawAndPitch.x)+", "+std::to_string(cameraYawAndPitch.y)+'\n').c_str(), stdout);*/
		return glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
	}

	glm::mat4 PerspectiveMatrix(){
		return glm::perspective(45.0f, windowRatio, 0.1f, 100.0f);
	}

};

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//std::cout << key << " " << scancode << " " << action << " " << mods <<std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	else if (key == GLFW_KEY_W) Application::GetInstance().keyW = action == GLFW_PRESS;
	else if (key == GLFW_KEY_S) Application::GetInstance().keyS = action == GLFW_PRESS;
	else if (key == GLFW_KEY_A) Application::GetInstance().keyA = action == GLFW_PRESS;
	else if (key == GLFW_KEY_D) Application::GetInstance().keyD = action == GLFW_PRESS;

	else if (key == GLFW_KEY_UP) Application::GetInstance().keyUp = action == GLFW_PRESS;
	else if (key == GLFW_KEY_DOWN) Application::GetInstance().keyDown = action == GLFW_PRESS;
	else if (key == GLFW_KEY_LEFT) Application::GetInstance().keyLeft = action == GLFW_PRESS;
	else if (key == GLFW_KEY_RIGHT) Application::GetInstance().keyRight = action == GLFW_PRESS;
}

static void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	Application & application = Application::GetInstance();
	if (application.jumpAtStartHappened == false){
		application.mousePosAtLastFrame = glm::vec2(xpos, ypos);
		application.jumpAtStartHappened = true;
	}
	glm::vec2 offset = application.mousePosAtLastFrame - glm::vec2(xpos, ypos);
	offset *= 0.05f; //mouse sensitivity
	application.cameraYawAndPitch += offset;
	glm::clamp(application.cameraYawAndPitch.y, -89.0f, 89.0f);	//restraining pitch to prevent weird bugs
	application.cameraFront = glm::normalize(
		glm::vec3(cos(glm::radians(application.cameraYawAndPitch.x)) * cos(glm::radians(application.cameraYawAndPitch.y)),
		sin(glm::radians(application.cameraYawAndPitch.y)),
		cos(glm::radians(application.cameraYawAndPitch.y)) * sin(glm::radians(application.cameraYawAndPitch.x)))
		);
}
