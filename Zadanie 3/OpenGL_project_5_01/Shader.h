#pragma once
#include <string>
#include "gl3w.h"
#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"

class Shader
{
public:
	Shader(const std::string& fileName);
	void Bind();

	GLuint CreateShader(std::string text, GLenum shaderType);
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	GLuint GetProgramID();

	static std::string FileToString(const std::string & fileName);
	virtual ~Shader();
private:
	static const unsigned int NUM_SHADERS = 2;

	GLuint mProgram;
	GLuint mShaders[NUM_SHADERS];
};

static glm::mat4 TransformationToMatrix(float translateX, float translateY, float translateZ, float rotationAngle, float scale){
	return  glm::translate(glm::vec3(translateX, translateY, translateZ))
		* glm::rotate(rotationAngle, glm::vec3(0.0f, 0.0f, 1.0f))
		* glm::scale(glm::vec3(scale, scale, scale));
}

