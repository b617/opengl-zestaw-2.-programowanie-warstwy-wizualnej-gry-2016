#include "stdafx.h"
#include "Mesh.h"


Mesh::Mesh(Vertex* vertices, unsigned int numVertices, GLuint drawingMode) : mDrawingMode(drawingMode), mDrawCount(numVertices)
{
	glGenVertexArrays(1, &mVertexArrayObject);
	glBindVertexArray(mVertexArrayObject);

		glGenBuffers(NUM_BUFFERS, mVertexArrayBuffers);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, numVertices*sizeof(vertices[0]),vertices,GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}

Mesh::Mesh(std::vector<Vertex> & vertices, GLuint drawingMode) : mDrawingMode(drawingMode)
{
	glGenVertexArrays(1, &mVertexArrayObject);
	glBindVertexArray(mVertexArrayObject);

		glGenBuffers(NUM_BUFFERS, mVertexArrayBuffers);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}


Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &mVertexArrayObject);
}

void Mesh::Draw() {
	glBindVertexArray(mVertexArrayObject);
		
		glDrawArrays(mDrawingMode, 0, mDrawCount);
		
	glBindVertexArray(0);
}

Mesh & Mesh::setDrawingMode(GLuint value) {
	mDrawingMode = value;
	return *this;
}


//////////////////////////////////////////////////

Mesh * BASIC_SHAPES::shapes2D::RegularPolygon(float radius, unsigned int verticesCount){
	return new Mesh(RegularPolygonVertices(radius,verticesCount), verticesCount, GL_TRIANGLE_FAN);
}
Vertex * BASIC_SHAPES::shapes2D::RegularPolygonVertices(float radius, unsigned int verticesCount){
	float angle = M_PI * 2 / verticesCount, currentAngle = 0.0f;
	Vertex * vertices = (Vertex*)malloc(sizeof(Vertex)*verticesCount);

	for (unsigned int i = 0; i < verticesCount; i++, currentAngle += angle) {
		vertices[i] = Vertex(glm::vec3(radius*cos(currentAngle), radius*sin(currentAngle), 0.0f));
	}
	return vertices;
}
Mesh * BASIC_SHAPES::shapes2D::Square(float radius){
	return RegularPolygon(radius, 4);
}
Mesh * BASIC_SHAPES::shapes2D::Triangle(float radius){
	return RegularPolygon(radius, 3);
}

MeshStructure * BASIC_SHAPES::shapes3D::Cone(float radius, float height, unsigned int numVertices){
	MeshStructure * result = new MeshStructure();
	Mesh * side, *base;
	float phi = 2 * M_PI / numVertices;

	Vertex* vert = (Vertex*)malloc(sizeof(Vertex)*(numVertices + 2));
	vert[0]=(Vertex(glm::vec3(0.0f, 0.0f, height)));
	for (int i = 0; i <= numVertices; i++){
		vert[i+1]=(Vertex(glm::vec3(cos(phi*i)*radius, sin(phi*i)*radius,0.0f)));
		//fputs((vert[i + 1].toString()+'\n').c_str(), stdout);
	}


	base = BASIC_SHAPES::shapes2D::RegularPolygon(radius,numVertices);
	side = new Mesh(vert, numVertices + 2, GL_TRIANGLE_FAN);

	result->objects.push_back(MeshWithShader(base, NULL));
	result->objects.push_back(MeshWithShader(side, NULL));
	return result;
}
MeshStructure * BASIC_SHAPES::shapes3D::Cylinder(float radius, float height, unsigned int numVertices){
	MeshStructure * result = new MeshStructure();
	Mesh * side, *base1, *base2;
	Vertex * base1Vert, *base2Vert, *sideVert;
	base1Vert = BASIC_SHAPES::shapes2D::RegularPolygonVertices(radius, numVertices);
	base2Vert = BASIC_SHAPES::shapes2D::RegularPolygonVertices(radius, numVertices);
	for (int i = 0; i < numVertices; i++) base2Vert[i].pos.z = height;
	sideVert = (Vertex*)malloc(sizeof(Vertex)* 2 * numVertices +2);

	for (int i = 0; i < numVertices * 2; i += 2) {
		sideVert[i] = base1Vert[i / 2];
		sideVert[i + 1] = base2Vert[i / 2];
	}
	sideVert[numVertices * 2] = base1Vert[0];
	sideVert[numVertices * 2 + 1] = base2Vert[0];

	base1 = new Mesh(base1Vert, numVertices, GL_TRIANGLE_FAN);
	base2 = new Mesh(base2Vert, numVertices, GL_TRIANGLE_FAN);
	side = new Mesh(sideVert, numVertices * 2 +2, GL_TRIANGLE_STRIP);

	result->objects.push_back(MeshWithShader(base1, NULL));
	result->objects.push_back(MeshWithShader(base2, NULL));
	result->objects.push_back(MeshWithShader(side, NULL));
	return result;
}
MeshStructure * BASIC_SHAPES::shapes3D::Sphere(float radius, unsigned int numVertices){
	MeshStructure * result = new MeshStructure();
	Mesh ** sphere = new Mesh*[numVertices];

	float step = 2 * radius / numVertices;
	for (int i = 0; i < numVertices; i++){
		float y = step*i - radius;
		Vertex * bottom = BASIC_SHAPES::shapes2D::RegularPolygonVertices(sqrt(radius*radius - y*y), numVertices);
		for (int j = 0; j < numVertices; j++) bottom[j].pos.z = y;
		y += step;
		Vertex * top = BASIC_SHAPES::shapes2D::RegularPolygonVertices(sqrt(radius*radius - y*y), numVertices);
		for (int j = 0; j < numVertices; j++) top[j].pos.z = y;
		Mesh * line = new Mesh(bottom, numVertices, GL_LINE_LOOP);
		result->objects.push_back(MeshWithShader(line, NULL));

		Vertex * sideVert = (Vertex*)malloc(sizeof(Vertex)* 2 * numVertices + 2);
		for (int j = 0; j < numVertices * 2; j += 2) {
			sideVert[j] = bottom[j / 2];
			sideVert[j + 1] = top[j / 2];
		}
		sideVert[numVertices * 2] = bottom[0];
		sideVert[numVertices * 2 + 1] = top[0];
		//delete[] bottom; delete[] top;

		sphere[i] = new Mesh(sideVert, numVertices * 2 + 2, GL_LINE_STRIP);
		result->objects.push_back(MeshWithShader(sphere[i], NULL));
	}

	return result;
}