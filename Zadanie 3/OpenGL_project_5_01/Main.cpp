#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

//---------------------------------------------------------
void StartGL();
void QuitGL();

const float radiusEarth = 1.0f;
const float velocityEarth = 1.2f;
const float velocityMoon = 8.0f;

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	Shader shader("shaders/myShader");
	GLuint shader_inColorLocation = glGetUniformLocation(shader.GetProgramID(), "inColor");
	GLuint shader_transformLocation = glGetUniformLocation(shader.GetProgramID(), "transform");

	MeshStructure* sphere = BASIC_SHAPES::shapes3D::Sphere(0.3f, 32);

	float time = 0.0;

	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////

		time = time + 0.01f; if (time > 2 * M_PI) time -= 2 * M_PI;

		glm::mat4 transformP = application.PerspectiveMatrix()*application.MoveCamera()*glm::mat4(1.0f);
		glm::mat4 transform = transformP * TransformationToMatrix(0,0,0,time*30,1.0f);

		//SUN
		glUniformMatrix4fv(shader_transformLocation, 1, GL_FALSE, &transform[0][0]);
		glUniform3f(shader_inColorLocation, 1.0f, 123.0f/256.0f, 8.0f/256.0f);
		shader.Bind();
		sphere->Draw();

		//EARTH
		glm::mat4 transformE = TransformationToMatrix(cos(time)*radiusEarth, sin(time)*radiusEarth, 0.0f, time * velocityEarth, .5f);
		transform = transformP*transformE;
		glUniformMatrix4fv(shader_transformLocation, 1, GL_FALSE, &transform[0][0]);
		glUniform3f(shader_inColorLocation, 65.0f / 256, 250.0f / 256, 93.0f / 256);
		shader.Bind();
		sphere->Draw();

		//MOON
		glm::mat4 transformM = TransformationToMatrix(cos(time)*radiusEarth, sin(time)*radiusEarth, 0.0f, time*velocityMoon, .3f);
		transform = transformP *transformM * transformE;
		glUniformMatrix4fv(shader_transformLocation, 1, GL_FALSE, &transform[0][0]);
		glUniform3f(shader_inColorLocation, 163.0 / 256, 168.0 / 256, 164.0 / 256);
		shader.Bind();
		sphere->Draw();


		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	//glfwSetCursorPosCallback(application.window, mouse_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0, 0, 0, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}